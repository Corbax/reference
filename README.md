
The purpose of this project is to index the different views and features of VLC to serve as a reference guide for development on the different platforms (mainly on desktop, android and iOS).

This reference document is structured around the main views of VLC, then, in each view, all the functionalities relating to it are listed.

[Link to Documentation](../../wikis/Index)
